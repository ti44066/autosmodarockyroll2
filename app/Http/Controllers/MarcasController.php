<?php

namespace App\Http\Controllers;

use App\Models\Marcas;
use Illuminate\Http\Request;

class MarcasController extends Controller
{

    public function index()
    {
        $marcas = Marcas::all();
        return response()->json($marcas);
    }

    public function store(Request $request)
    {
        $rules = ['marca'=>'required|string|min:1|max:50'];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' =>false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $marcas = new Marcas($request->input());
        $marcas ->save();
        return response()->json([
            'status' =>true,
            'message' => 'Se guardo correctamente'
        ],200);
    }

    public function show(Marcas $marcas)
    {
        return response()->json(['status' => true, 'data' => $marcas]);
    }

    public function update(Request $request, Marcas $marcas)
    {
        $rules = ['marca'=>'required|string|min:1|max:50'];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $marcas->update($request->input());
        return response()->json([
            'status' => true,
            'message' => "Se actualizo correctamente"
        ],200);
    }

    public function destroy(Marcas $marcas)
    {
        $marcas->delete();
        return response()->json([
            'status' => true,
            'message' => "Se elimino correctamente"
        ],200);
    }
}
