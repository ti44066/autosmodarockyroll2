<?php

namespace App\Http\Controllers;

use App\Models\Autos;
use App\Models\Marcas;
use Illuminate\Http\Request;

class AutosController extends Controller
{

    public function index()
    {
        $autos = Autos::select('autos.*','marcas.marca')->join('marcas','marcas.id','=','autos.marca_id')->paginate(10);
        return response()->json($autos);
    }

    public function store(Request $request)
    {
        $rules = [
            'marca_id' => 'required|numeric',
            'submarca' => 'required|string|min:1|max:60',
            'anio' => 'required|numeric',
            'tipo' => 'required|string|min:1|max:20',
            'color' => 'required|string|min:1|max:40'
        ];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $autos = new Autos($request->input());
        $autos-save();
        return response()->json([
            'status' => true,
            'message' => "Se guardo correctamente"
        ],200);
    }

    public function show(Autos $autos)
    {
        return response()->json(['status' => true, 'date' => $autos]); 
    }

    public function update(Request $request, Autos $autos)
    {
        $rules = [
            'marca_id' => 'required|numeric',
            'submarca' => 'required|string|min:1|max:60',
            'anio' => 'required|numeric',
            'tipo' => 'required|string|min:1|max:20',
            'color' => 'required|string|min:1|max:40'
        ];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $autos->update($request->input());
        return response()->json([
            'status' => true,
            'message' => "Se auctualizo correctamente"
        ],200);
    }

    public function destroy(Autos $autos)
    {
        $autos->delete();
        return response()->json([
            'status' => true,
            'message' => "Se elimino correctamente"
        ],200);
    }
    public function AutoByMarca(){
        $autos = Autos::select(DB::raw('count(autos.id as count)','marcas.marca'))->join('marcas','marcas.id','=','autos.marca_id')->groupBy('marcas.marca')->get();
        return response()->json($autos);
    }
    public function all(){
        $autos = Autos::select('autos.*','marcas.marca')->join('marcas','marcas.marca','=','autos.marca_id')->get();
        return response()->json($autos);
    }
}
