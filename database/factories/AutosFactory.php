<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Autos>
 */
class AutosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'marca_id' => $this -> faker -> numberBetween(1,2),
            'submarca' => $this -> faker -> name,
            'anio' => $this -> faker -> numberBetween(1950,2025),
            'tipo' => $this -> faker -> name,
            'color' => $this -> faker -> name
        ];
    }
}
